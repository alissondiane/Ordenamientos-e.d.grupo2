// Tarea_Ordenamiento.cpp: define el punto de entrada de la aplicación de consola.
//

#include "stdafx.h"


#include <iostream>
#include <stdio.h>
#include <stdlib.h>

using namespace std;
int cont = 0;


struct Nodo{
	int dato;
	struct Nodo* siguiente;
	struct Nodo* anterior;
};

typedef struct Nodo* PLista;


PLista CrearNodo(int dato){
	PLista p = new (struct Nodo);
	p->dato = dato;
	p->siguiente = NULL;
	p->anterior = NULL;
	return p;

}

void RecorrerLista(PLista &lista){

	PLista p = lista;
	while (p != NULL){
		cout << p->dato << "->";
		p = p->siguiente;

	}
	cout << "null\n\n" << endl;

}

void InsertarPrincipio(PLista &lista, int dato){
	PLista nuevo = CrearNodo(dato);

	if (lista == NULL){

		lista = nuevo;

	}
	else{
		nuevo->siguiente = lista;
		nuevo->anterior = NULL;
		lista->anterior = nuevo;
		lista = nuevo;
	}
	cont = cont + 1;
}
void InsertarFinal(PLista &lista, int dato){
	PLista nuevo = CrearNodo(dato);
	if (lista == NULL){
		lista = nuevo;
	}
	else{
		PLista aux = lista;

		while (aux->siguiente != NULL){
			aux = aux->siguiente;
		}
		aux->siguiente = nuevo;
		nuevo->anterior = aux;
		nuevo->siguiente = NULL;

	}
	cont = cont + 1;
}
void InsertarPorPosicion(PLista &lista, int dato, int pos){
	PLista nuevo = CrearNodo(dato);
	if (lista == NULL){
		cout << "La lista esta vacio no se puede agregar" << endl;
	}
	else{
		if (pos <= cont && pos>0){
			if (pos == 1){
				InsertarPrincipio(lista, dato);
			}
			else{
				int n = 1;
				PLista aux = lista;
				while (n != pos){
					aux = aux->siguiente;
					n++;
				}
				nuevo->siguiente = aux;
				nuevo->anterior = aux->anterior;
				aux->anterior = nuevo;
				nuevo->anterior->siguiente = nuevo;
				cont = cont + 1;
			}
		}
		else{
			cout << "posicion invalida" << endl;
		}
	}
}
void EliminarPrincipio(PLista &lista){
	PLista aux = lista;
	if (aux != NULL){
		PLista temporal = lista;
		if (aux->siguiente == NULL){
			lista = NULL;
		}
		else{
			lista = aux->siguiente;
			lista->anterior = NULL;
		}
		delete temporal;
		cont = cont - 1;
	}

}
void EliminarFinal(PLista &lista){
	if (lista != NULL){
		PLista aux = lista, temporal = NULL;
		if (aux->siguiente == NULL){
			aux->anterior = NULL;
			lista = NULL;
			temporal = aux;
		}
		else{
			while (aux->siguiente != NULL){
				aux = aux->siguiente;
			}
			aux->anterior->siguiente = NULL;
			aux->anterior = NULL;
			temporal = aux;
		}
		delete temporal;
		cont = cont - 1;

	}
}
void EliminarPorPosicion(PLista &lista, int pos){
	PLista p = lista;
	int n = 1;
	if (lista != NULL){
		if (pos <= cont && pos>0){
			if (pos == 1){
				EliminarPrincipio(lista);
			}
			else{
				while (n != pos){
					p = p->siguiente;
					n++;
				}
				if (p->siguiente != NULL){
					p->anterior->siguiente = p->siguiente;
					p->siguiente->anterior = p->anterior;
					delete p;
				}
				else{
					p->anterior->siguiente = NULL;
					delete p;
				}
				cont--;
			}
		}
		else{
			cout << "posicion invalida:" << endl;

		}
	}
	else{
		cout << "lista vacia" << endl;
	}

}
int CantidadDeElementos(PLista &lista){
	PLista p = lista;
	int cantidad = 1;
	while (p->siguiente != NULL){
		p = p->siguiente;
		cantidad = cantidad + 1;
	}
	return cantidad;
}
void OrdenamientoBurbuja(PLista &lista){
	bool fin = false;
	int dato;
	int i = 1;
	while (i < cont && !fin){
		fin = true;
		PLista aux = lista;
		for (int k = 1; k <= cont - i; k++){
			if (aux->dato > aux->siguiente->dato){
				dato = aux->siguiente->dato;
				aux->siguiente->dato = aux->dato;
				aux->dato = dato;
				fin = false;
			}
			aux = aux->siguiente;
		}
		i = i + 1;
	}


}
void OrdenamientoBurbuja2(PLista &lista){
	char sw = 'F'; PLista aux = lista;
	int i = 1;
	while (sw == 'F'){
		sw = 'V';
		while (aux->siguiente != NULL){
			if (aux->dato > aux->siguiente->dato){
				int dato = aux->dato;
				aux->siguiente->dato = aux->dato;
				aux->dato = dato;
				sw = 'F';
			}
			aux = aux->siguiente;
		}
		aux = lista;
	}

}
void OrdenamientoPorInsercion(PLista &lista, int dato){
	PLista nuevo = CrearNodo(dato);
	PLista p = lista;

	if (p == NULL){
		lista = nuevo;
		cont = cont + 1;
	}
	else{
		if (dato < p->dato){
			InsertarPrincipio(lista, dato);
		}
		else{
			PLista aux = lista;
			bool encontrado = false;
			int i = 1;
			while (aux->siguiente != NULL && !encontrado){

				if (dato >= aux->dato && dato <= aux->siguiente->dato){
					InsertarPorPosicion(lista, dato, i + 1);
					encontrado = true;
				}
				aux = aux->siguiente;
				i = i + 1;
			}
			if (aux->siguiente == NULL){
				InsertarFinal(lista, dato);

			}


		}

	}

}
void OrdenarPorIntercambio(PLista &lista){
	PLista p = lista, dato = NULL, x = NULL;
	while (p->siguiente != NULL){
		x = p;
		PLista aux = p;
		while (aux->siguiente != NULL){
			if (x->dato > aux->siguiente->dato){
				int a = aux->siguiente->dato;
				aux->siguiente->dato = x->dato;
				x->dato = a;
			}
			aux = aux->siguiente;
		}
		p = p->siguiente;
	}
}
void OrdenarPorSeleccion(PLista &lista){
	PLista p = lista, min = NULL, aux = NULL;
	while (p->siguiente != NULL){
		min = p;
		PLista aux = p->siguiente;
		while (aux != NULL){//recorro la lista
			if (aux->dato < min->dato){
				min = aux;
			}
			aux = aux->siguiente;
		}

		int a = p->dato;
		p->dato = min->dato;
		min->dato = a;
		p = p->siguiente;

	}

}
void burbuja(PLista &lista){
	bool inter = false;
	PLista p, q;
	do{
		p = lista;
		inter = false;
		while (p->siguiente != NULL){
			q = p->siguiente;
			if (p->dato>q->dato){
				int aux = q->dato;
				q->dato = p->dato;
				p->dato = aux;
				inter = true;

			}
			p = p->siguiente;
		}
	} while (inter == true);
}
PLista MergeSort(PLista lista){
	PLista p = lista;
	PLista p1 = lista;
	int tam = 1;
	while (p1->siguiente != NULL){
		p1 = p1->siguiente;
		tam++;
	}
	//cout<<tam<<endl;
	if (tam == 1){
		return p;
	}
	else{
		int medio = tam / 2;
		int i = 1;
		p1 = p;
		while (i < medio){
			p1 = p1->siguiente;
			i++;
		}
		PLista mitad = p1->siguiente;
		p1->siguiente = NULL;

		PLista p2 = NULL; PLista p3 = NULL;
		p2 = MergeSort(p);
		p3 = MergeSort(mitad);

		PLista nuevo = NULL; PLista aux = NULL;
		if (p2->dato <= p3->dato){
			nuevo = p2;
			p2 = p2->siguiente;
			aux = nuevo;
		}
		else{
			nuevo = p3;
			p3 = p3->siguiente;
			aux = nuevo;
		}

		while (p2 != NULL && p3 != NULL){
			if (p2->dato <= p3->dato){
				aux->siguiente = p2;
				p2 = p2->siguiente;
				aux = aux->siguiente;
			}
			else{
				aux->siguiente = p3;
				p3 = p3->siguiente;
				aux = aux->siguiente;
			}
		}
		if (p2 == NULL){
			aux->siguiente = p3;
		}
		else{
			aux->siguiente = p2;
		}
		RecorrerLista(nuevo);
		return nuevo;
	}
}
PLista saltos(PLista q, int n){
	while (n != 0){
		q = q->siguiente;
		n = n - 1;
	}
	return q;
}


void Shell(PLista & lista, int cont){
	int numSal;
	PLista p;
	PLista q = new (struct Nodo);
	p = lista;
	numSal = cont;
	bool e = false;

	do{
		e = false;

		numSal = int(numSal / 2);


		while (q != NULL){
			q = saltos(p, numSal);
			if (q != NULL){


				if (p->dato > q->dato){
					int aux;
					aux = p->dato;
					p->dato = q->dato;
					q->dato = aux;
					e = true;

				}
				p = p->siguiente;

			}

		}
		if (numSal>0 && e){
			Shell(lista, 2);
		}
		else if (e){
			Shell(lista, 2);
		}

	} while (numSal >1);
}


void QuickSort(PLista &lista, PLista &ultimo){
	PLista p = lista;
	PLista pivote;
	int i = 1;
	int longitud = 1;
	while (p != ultimo){
		p = p->siguiente;
		longitud++;
	}

	if (longitud != 1){

		p = lista;
		while (i <= (longitud / 2)){
			p = p->siguiente;
			i++;
		}
		pivote = p;

		PLista inicio = lista;

		PLista fin;
		i = 1;
		p = lista;
		while (i < longitud){
			p = p->siguiente;
			i++;
		}
		fin = p;

		int aux;
		while (fin != pivote || inicio != pivote){

			while (fin != pivote && inicio != pivote){
				if (inicio->dato > pivote->dato && fin->dato < pivote->dato){
					aux = inicio->dato;
					inicio->dato = fin->dato;
					fin->dato = aux;
					inicio = inicio->siguiente;
					fin = fin->anterior;

				}
				else{
					if (inicio->dato <= pivote->dato){
						inicio = inicio->siguiente;
					}
					if (fin->dato > pivote->dato){
						fin = fin->anterior;
					}
				}
			}

			if (fin == pivote){
				if (inicio->dato > pivote->dato){
					aux = pivote->dato;
					pivote->dato = inicio->dato;
					inicio->dato = aux;
					pivote = inicio;
					fin = fin->anterior;
				}
				else{
					if (inicio != pivote){
						inicio = inicio->siguiente;
					}

				}
			}

			if (inicio == pivote){
				if (fin->dato < pivote->dato){
					aux = pivote->dato;
					pivote->dato = fin->dato;
					fin->dato = aux;
					pivote = fin;
					inicio = inicio->siguiente;
				}
				else{
					if (fin != pivote){
						fin = fin->anterior;
					}

				}
			}

		}

		p = lista;
		if (pivote->siguiente != NULL && pivote != ultimo){
			QuickSort(pivote->siguiente, ultimo);
		}
		if (pivote->anterior != NULL && pivote != ultimo){
			QuickSort(p, pivote->anterior);
		}
	}
}
int valorMax(PLista &lista){
	PLista p = lista;
	int maxi = p->dato;
	while (p != NULL){
		if (p->dato> maxi)
			maxi = p->dato;
		p = p->siguiente;
	}
	return maxi;
}

int numDecimales(int maxi){
	int t = 0;
	while (maxi>0)
	{
		t = t + 1;
		maxi = maxi / 10;
	}
	return t;
}



void Radix(PLista &lista){
	int   d, m = 1, t = 0, maxi = 0;
	PLista temp, cabeza[10], cola[10];

	maxi = valorMax(lista);

	t = numDecimales(maxi);

	for (int j = 1; j <= t; j++)
	{

		for (int i = 0; i <= 9; i++)
		{
			cabeza[i] = NULL;
			cola[i] = NULL;
		}


		while (lista != NULL)
		{
			d = static_cast<int>(lista->dato / m) % 10;
			temp = lista;
			lista = lista->siguiente;
			temp->siguiente = NULL;
			if (cabeza[d] != NULL)
			{
				cola[d]->siguiente = temp;
				cola[d] = temp;
			}
			else {

				cabeza[d] = temp;
				cola[d] = temp;
			}
		}
		d = 0;
		while (cabeza[d] == NULL) {
			d++;
		}

		lista = cabeza[d];
		temp = cola[d];
		d++;

		while (d<10){
			if (cabeza[d] != NULL){
				temp->siguiente = cabeza[d];
				temp = cola[d];
			}

			d++;
		}
		m = m * 10;
	}
}




int menu(){
	int opc;
	cout << "   " << endl;
	cout << "\t\t\t\tMENU" << endl;
	cout << "\t\t------------------------------------" << endl;
	cout << "\t\t\tElija una opcion\n\n" << endl;
	cout << "\t\t 1. Ordenamiento por burbuja" << endl;
	cout << "\t\t 2. Ordenamiento por insercion" << endl;
	cout << "\t\t 3. Ordenamiento por intercambio" << endl;
	cout << "\t\t 4. Ordenamiento por seleccion" << endl;
	cout << "\t\t 5. Ordenamiento Shell" << endl;
	cout << "\t\t 6. Ordenamiento Quicksort" << endl;
	cout << "\t\t 7. Ordenamiento Mergesort" << endl;
	cout << "\t\t 8. Ordenamiento Radix" << endl;
	cout << "\t\t 0. Salir \n\n" << endl;
	cout << "Ingrese su opcion: \n\n";
	cin >> opc;

	return  opc;

}





//int main()
//{
int main(array<System::String ^> ^args)
{
	/*	return 0;
	}*/
	PLista lista, lista1, lista2, lista3, lista4, lista5, lista6, lista7;
	PLista pos = NULL;
	char rpt;
	int opc;
	do {
		opc = menu();
		switch (opc){
		case 1: cont = 0;
			lista = NULL;
			int dato;
			do{
				cout << "ingrese el dato:";
				cin >> dato;
				InsertarFinal(lista, dato);
				RecorrerLista(lista);
				cout << "Desea continuar?S/N:";;
				cin >> rpt;
			} while (rpt == 's' || rpt == 'S');

			cout << "Lista ordenada por Burbuja:" << endl;
			burbuja(lista);
			RecorrerLista(lista);

			break;

		case 2:
			cont = 0;
			lista1 = NULL;
			int dato1;
			do{
				cout << "ingrese el dato:";
				cin >> dato1;
				OrdenamientoPorInsercion(lista1, dato1);
				RecorrerLista(lista1);
				cout << "Desea continuar?S/N:";;
				cin >> rpt;
			} while (rpt == 's' || rpt == 'S');
			cout << "lista ordenada por Insercion:" << endl;
			RecorrerLista(lista1);
			break;

		case 3:
			cont = 0;

			lista2 = NULL;
			int dato2;
			do{
				cout << "ingrese el dato:";
				cin >> dato2;
				InsertarFinal(lista2, dato2);
				RecorrerLista(lista2);
				cout << "Desea continuar?S/N";
				cin >> rpt;
			} while (rpt == 's' || rpt == 'S');
			cout << "Lista ordenada por Intercambio:" << endl;
			OrdenarPorIntercambio(lista2);
			RecorrerLista(lista2);

			break;



		case 4:  cont = 0;
			lista3 = NULL;
			int dato3;
			do{
				cout << "ingrese el dato:";
				cin >> dato3;
				InsertarFinal(lista3, dato3);
				RecorrerLista(lista3);
				cout << "Desea continuar?S/N";
				cin >> rpt;
			} while (rpt == 's' || rpt == 'S');
			cout << "Lista ordenada por Seleccion:" << endl;
			OrdenarPorSeleccion(lista3);
			RecorrerLista(lista3);
			break;
		case 5:

			cont = 0;

			lista4 = NULL;
			int dato4;
			do{
				cout << "ingrese el dato:";
				cin >> dato4;
				InsertarFinal(lista4, dato4);
				RecorrerLista(lista4);
				cout << "Desea continuar?S/N";
				cin >> rpt;
			} while (rpt == 's' || rpt == 'S');
			cout << "Lista ordenada por Shell:" << endl;
			Shell(lista4, cont);
			RecorrerLista(lista4);

			break;
		case 6:
			cont = 0;

			lista5 = NULL;
			int dato5;
			do{
				cout << "ingrese el dato:";
				cin >> dato5;
				InsertarFinal(lista5, dato5);
				RecorrerLista(lista5);
				cout << "Desea continuar?S/N";
				cin >> rpt;
			} while (rpt == 's' || rpt == 'S');
			cout << "Lista ordenada por Quicksort:" << endl;
			pos = lista5;
			while (pos->siguiente != NULL){
				pos = pos->siguiente;
			}
			QuickSort(lista5, pos);
			RecorrerLista(lista5);

			break;
		case 7:
			cont = 0;

			lista6 = NULL;
			int dato6;
			do{
				cout << "ingrese el dato:";
				cin >> dato6;
				InsertarFinal(lista6, dato6);
				RecorrerLista(lista6);
				cout << "Desea continuar?S/N";
				cin >> rpt;
			} while (rpt == 's' || rpt == 'S');
			cout << "Lista ordenada por Mergesort:" << endl;
			lista6 = MergeSort(lista6);
			RecorrerLista(lista6);

			break;
		case 8:
			cont = 0;

			lista7 = NULL;
			int dato7;
			do{
				cout << "ingrese el dato:";
				cin >> dato7;
				InsertarFinal(lista7, dato7);
				RecorrerLista(lista7);
				cout << "Desea continuar?S/N";
				cin >> rpt;
			} while (rpt == 's' || rpt == 'S');
			cout << "Lista ordenada por Radix:" << endl;
			Radix(lista7);
			RecorrerLista(lista7);

			break;


		}

		system("pause");  system("cls");
	} while (opc != 0);



	system("pause");
	return(0);

}

